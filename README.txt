RELEASE NOTES - GridUnit 1.1


Hi, this is the first SourceForge version of the GridUnit Testing Framework.

There are two versions available for download.

The first version, packed in the file gridunit-1.1.zip is a standalone version of the framework.
If you download this version you will be able to execute a JUnit TestSuite in your own machine using a 
multithreaded test runner.

Ths second version, packed in the file gridunit-ourgrid-1.1.zip, includes the standalone version and
provides also a OurGridTestRunner. This test runner is able to distribute the execution of a JUnit TestSuite
over the OurGrid Community.

In order to use the OurGridTestRunner you must first download and install the OurGrid software pack from www.ourgrid.org.
The OurGrid configure + install process should be very easy, specially if you take a look at this document (http://www.ourgrid.org/twiki-public/bin/view/OG/OurGettingStarted).


The use of both testrunners is similar.
You should invoke the class gridunit.ui.swingui.GridUnit with two command line parameters:
   * YourTestSuiteClassName: is the fully qualified name of a class that extends JUnit's TestSuite and contains all your test cases.
   * ATestRunner: is the fully qualified name of the test runner you want to use. 
     In this versions it should be eithergridunit.testrunners.local.LocalTestRunner or 
     gridunit.testrunners.ourgrid.OurGridTestRunner.


Additionaly you should define one system property called gridunit.runtime with the path of the file gridunit_runtime.jar include in the releases.


Example:

To use the GridUnit to run the tests contained in the TestSuite class myapp.XYZTestSuite using the LocalTestRunner you should type:
java -classpath=yourclasspathincludingthegridunitlibraries -Dgridunit.runtime=pathtogridunit_runtime.jar myapp.XYZTestSuite gridunit.testrunners.local.LocalTestRunner

In a similar way, to run the same test suite in the OurGrid Community you should type:
java -classpath=yourclasspathincludingthegridunitlibraries -Dgridunit.runtime=pathtogridunit_runtime.jar myapp.XYZTestSuite gridunit.testrunners.ourgrid.OurGridTestRunner

Remember that both commands should be typed inside your application directory and that to use the OurGridTestRunner you must have the OurGrid solution installed and running in your machine.

Feel free to contact us for help at http://gridunit.sourceforge.net. 
All feedback about the GridUnit usage will be very welcomed.

The GridUnit Team

