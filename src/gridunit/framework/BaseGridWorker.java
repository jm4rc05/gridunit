/*
 * Copyright (c) 2002-2005 Universidade Federal de Campina Grande
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 * For more information: http://gridunit.sourceforge.net
 */
package gridunit.framework;

import junit.framework.TestCase;

/**
 * Description: This class easily the creation of new GridWorkers.
 * 
 * @author Alexandre Duarte - alex@dsc.ufcg.edu.br
 */
public abstract class BaseGridWorker implements GridWorker {

    protected TestCase test;
    
    protected GridTestListener listener;
    
    
    /* (non-Javadoc)
     * @see gridunit.testrunners.GridWorker#setTest(junit.framework.TestCase)
     */
    public void setTest(TestCase t) {
        this.test = t;
        
    }

    /*
     *  (non-Javadoc)
     * @see gridunit.testrunners.GridWorker#setGridTestListener(gridunit.framework.GridTestListener)
     */
    public void setGridTestListener(GridTestListener listener) {
        this.listener = listener;
        
    }

}
