/*
 * Copyright (c) 2002-2005 Universidade Federal de Campina Grande
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 * For more information: http://gridunit.sourceforge.net
 */
package gridunit.framework;


import java.util.Collection;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Vector;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import org.smartfrog.services.junit.TestInfo;

import EDU.oswego.cs.dl.util.concurrent.PooledExecutor;

/**
 * 
 * Description: This abstract class provides an easy start up to build a real grid test runner.
 * 
 * @author Alexandre Duarte - alex@dsc.ufcg.edu.br
 */
public abstract class BaseGridTestRunner implements GridTestRunner, GridTestListener {

	/**
	 * A collection os listeners.
	 */
    private Collection gridTestListeners;

    /**
     * A Thread pool.
     */
    private PooledExecutor pool;
    
    
    /**
     * A collection of GridWorkers.
     */
    private Collection workers;
    
    /**
     * A Factory of gridworkers.
     */
    private GridWorkerFactory factory;
    
    
    /**
     * Full Constructor for BaseGridTestRunner.
     */
    public BaseGridTestRunner( GridWorkerFactory factory ) {
        
        gridTestListeners = new Vector();
        workers = new Vector();
        pool = new PooledExecutor();
        pool.setMaximumPoolSize(100);
        this.factory = factory;
        
    }

    /**
     * Runs all tests in a given test suite.
     * @param suite The test suite.
     */
    public void run(TestSuite suite) {
                
        notifyStarted();
               
        innerRun( suite );
        
        waitUntilWorkIsDone();
        notifyFinished();
        
    }
    
    /**
     * Accessory run method.
     * @param suite The test suite.
     */
    private void innerRun( TestSuite suite ) {
        
        Enumeration enum = suite.tests();
        while (enum.hasMoreElements()) {
            
            Object o = enum.nextElement();
            
            if( o instanceof TestCase ) {
            
                TestCase t = (TestCase) o;

                GridWorker worker = factory.createGridWorker();
                worker.setTest( t );
                worker.setGridTestListener( this );
                workers.add(worker);

                try {
                    pool.execute(worker);
                } catch (InterruptedException e1) {
                    
                }
            } else if( o instanceof TestSuite ) {
                innerRun( (TestSuite) o );
            }
                
        }

    }

    /**
     * Blocks application until work is done.
     */
    private void waitUntilWorkIsDone() {
        
        boolean done = false;
        while (!done) {

            try {
                Thread.sleep((long) (2000 * Math.random()));
            } catch (InterruptedException e) {
            }
            done = true;

            synchronized (workers) {
                Iterator it = workers.iterator();
                while (it.hasNext()) {
                    if (!((GridWorker) it.next()).workIsDone()) {
                        done = false;
                        break;
                    }
                }
            }
        }

    }

   /**
    * Adds a GridTestListener interested in this GridTestRunner events.
    */
    public void addGridTestListener(GridTestListener aGridTestListener) {
        
        synchronized (gridTestListeners) {
            gridTestListeners.add(aGridTestListener);
        }
    }

    /**
     * Called when a test is finished.
     * @param testInfo The test info.
     */
    public void testFinished( TestInfo testInfo ) {
        synchronized (gridTestListeners) {
            Iterator it = gridTestListeners.iterator();
            while (it.hasNext()) {

                GridTestListener gt = (GridTestListener) it.next();
                gt.testFinished(testInfo );

            }
        }

    }

    /**
     * Called when a test has been started
     * @param test The test.
     */
    public void testStarted( Test test) {
        
        synchronized (gridTestListeners) {
            Iterator it = gridTestListeners.iterator();
            while (it.hasNext()) {

                GridTestListener gt = (GridTestListener) it.next();
                gt.testStarted(test);

            }
        }
        
    }

    /**
     * Called when a a test has failed due to an unantecipated error.
     * @param testInfo The test info.
     */
    public void testError( TestInfo testInfo ){
        
        synchronized (gridTestListeners) {
            Iterator it = gridTestListeners.iterator();
            while (it.hasNext()) {

                GridTestListener gt = (GridTestListener) it.next();
                gt.testError(testInfo);
            }
        }
        
    }
    

    /**
     * Called when a test has failed due to an unsatisfied assertion
     * @param testInfo The test info.
     */
    public void testFailed( TestInfo testInfo ) {
        
        synchronized (gridTestListeners) {
            Iterator it = gridTestListeners.iterator();
            while (it.hasNext()) {

                GridTestListener gt = (GridTestListener) it.next();
                gt.testFailed(testInfo);

            }
        }

    }

    /**
     * Called when the test phase has been started.
     */
    public void testPhaseStarted(){
    }
    
    
    /**
     * Called when all tests where executed (sucessfully or unsucessfully).
     */
    public void testPhaseFinished() {
    }
  
    /**
     *  Notifies all listeners a test phase started.
     */
    private void notifyStarted() {
        
        Iterator it = gridTestListeners.iterator();
        while( it.hasNext() ) {
            GridTestListener gtl = (GridTestListener) it.next();
            gtl.testPhaseStarted();
        }
      
    }
    
    /**
     * Notifies all listeners a test phase finished.
     */
    private void notifyFinished() {
        Iterator it = gridTestListeners.iterator();
        while( it.hasNext() ) {
            GridTestListener gtl = (GridTestListener) it.next();
            gtl.testPhaseFinished();
        }
    }

    /**
     * Stops the test execution.
     */
    public void stop() {
        synchronized( workers ) {
            Iterator it = workers.iterator();
            while( it.hasNext() ) {
                GridWorker worker = (GridWorker) it.next();
                worker.stop();
            }
        }
    }

}