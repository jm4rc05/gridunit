/*
 * Copyright (c) 2002-2005 Universidade Federal de Campina Grande
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 * For more information: http://gridunit.sourceforge.net
 */
package gridunit.ui.swingui;

import java.util.HashMap;
import java.util.Map;

import javax.swing.ImageIcon;
import javax.swing.JFrame;


/**
 * 
 * Description: This is an IconManager :-)
 * 
 * 
 * @author Alexandre Duarte - alex@dsc.ufcg.edu.br
 */
public class IconManager {
    
    private static Map icons;
    
    public static final String classIcon = "class";
    public static final String packageIcon = "package";
    public static final String gridunitIcon = "gridunit";
    public static final String testIcon = "test";
    public static final String errorIcon = "error";
    public static final String failureIcon = "failure";
    public static final String hierarchyIcon = "hierarchy";
    public static final String infoIcon = "info";
    public static final String traceIcon = "trace";
    public static String finishedIcon = "ok";
    public static String runningIcon = "running";
    
    
    public static synchronized ImageIcon getIcon( String name ) {

        if( icons == null ) {
            createMapIcon();
        }
        
        return (ImageIcon) icons.get(name);
        
    }
    
    private static void createMapIcon() {

        icons =  new HashMap();
        icons.put( classIcon , new ImageIcon(JFrame.class.getResource("/icons/class.gif") ) );
        icons.put( packageIcon, new ImageIcon(JFrame.class.getResource("/icons/package.gif")));
        icons.put( gridunitIcon , new ImageIcon(JFrame.class.getResource("/icons/gridunit.gif")));
        icons.put( testIcon, new ImageIcon(JFrame.class.getResource("/icons/gridtest.gif")));
        icons.put( errorIcon , new ImageIcon(JFrame.class.getResource("/icons/error.gif")));
        icons.put( failureIcon , new ImageIcon(JFrame.class.getResource("/icons/failure.gif")));
        icons.put( hierarchyIcon , new ImageIcon(JFrame.class.getResource("/icons/hierarchy.gif")));
        icons.put( infoIcon , new ImageIcon(JFrame.class.getResource("/icons/info.gif")));
        icons.put( traceIcon , new ImageIcon(JFrame.class.getResource("/icons/trace.gif")));
        icons.put( finishedIcon , new ImageIcon(JFrame.class.getResource("/icons/ok.gif")));
        icons.put( runningIcon , new ImageIcon(JFrame.class.getResource("/icons/running.gif")));
        
        
    }
    
}
