/*
 * Copyright (c) 2002-2005 Universidade Federal de Campina Grande
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 * For more information: http://gridunit.sourceforge.net
 */
package gridunit.testrunners.local;

import gridunit.framework.BaseGridTestRunner;

/**
 * Description: The LocalTestRunner is a class able to execute all tests in a JUnit TestSuite locally.
 * This works exactly as JUnit.
 * 
 * @author Alexandre Duarte - alex@dsc.ufcg.edu.br
 */
public class LocalTestRunner extends BaseGridTestRunner {
    
    /**
     * Creates a LocalTestRunner
     */
    public LocalTestRunner() {
        
    	super( new LocalWorkerFactory() );
    	
    }
   
}